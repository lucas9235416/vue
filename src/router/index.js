import { createRouter, createWebHistory } from 'vue-router';
import Home from '../components/Home.vue';
import PostDetail from '../components/PostDetail.vue';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/post/:id',
    name: 'PostDetail',
    component: PostDetail
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes
});

export default router;
